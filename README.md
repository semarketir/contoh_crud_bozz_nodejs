# NodeJS ( Mongo + Express : CRUD )
Contoh kode CRUD ke mongodb

**Install heula kabeh depedensi module nu di pake**
```sh
npm install --save
```

**Jalankeun**
```sh
DEBUG=bozz:* bin/www
```

**PORT Default**
```sh
3000
```

**Buka file App.js**
```sh
Setting host + dbname mongo
```

**CRUD User Collections**
```sh
METHOD  : RESOURCES
GET     : /users/
GET     : /users/:username
POST    : /users
PUT     : /users/:username
DELETE  : /users/:username
```

**Alat tempur**
```sh
https://www.mongodb.org
https://robomongo.org
http://expressjs.com
http://mongoosejs.com
```

```Testing di NodeJS 4 LTS```

LICENSE
---
WTPFL