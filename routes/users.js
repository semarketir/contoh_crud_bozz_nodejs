var express = require('express'),
	router	= express.Router(),
	User	= require('../models/modelUser');

router.get('/', function(req, res, next) {
	User.find({}, function(err, users) {
		if (err ) {
			res.send( err );
		}else{
			res.send(users);
		};
	});
});

router.get('/:username', function(req, res, next) {
	//User.findById(__ID_MONGO_COLLECTION,..)
	User.findOne({ username : req.params.username}, function(err, user) {
		if (err ) {
			res.send( err );
		}else{
			res.send(user);
		};
	});
});

router.post('/', function(req, res, next) {
	var newUser = User({
		name		: req.body.name,
		username	: req.body.username,
		password	: req.body.password,
		admin		: req.body.admin
	});
	
	newUser.save(function(err) {
		if (err ) {
			res.send( err );
		}else{
			res.send({success:true});
		};
	});
});

//update nama hungkul, bebas sok naon we update
router.put('/:username', function(req, res, next) {
	User.findOneAndUpdate({ username : req.params.username}, { name : req.body.name}, function(err, user) {
		if (err ) {
			res.send( err );
		}else{
			res.send({success:true});
		};
	});
});

router.delete('/:username', function(req, res, next) {
	User.findOneAndRemove({ username : req.params.username}, function(err, user) {
		if (err ) {
			res.send( err );
		}else{
			res.send({success:true});
		};
	});
});

module.exports = router;
